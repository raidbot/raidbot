var Discord = require('discord.io');
var logger = require('winston');
var auth = require('./auth.json');
var roster = require('./roster.json');
var parsed = require('./parsed.json');
var join = require('./joining.json');
var joiningIdx = 0;
var leave = require('./leaving.json');
var leavingIdx = 0;
var count = 0;
var found = false;

//Initialize roster
for ( var x = 0; x < 24; x++) {
    roster.id[x] = null;
    roster.added[x] = false;
    roster.var1[x] = null;
    roster.var2[x] = null;
    roster.var3[x] = null;
    roster.var4[x] = null;
    roster.var5[x] = null;
    roster.var6[x] = null;
    roster.var7[x] = null;
    roster.var8[x] = null;
    roster.role[x] = null;
}
// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true});
logger.level = 'debug';
// Initialize Discord Bot
var bot = new Discord.Client({
    token: auth.token,
    autorun: true
});

bot.on('ready', function (evt) {
    console.log('Bot launched...');
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' � (' + bot.id + ')');
});

bot.on('message', function (user, userID, channelID, message, evt) {

    if (message.substring(0, 1) == '!') {
        var args = message.substring(1).split(' ');
        var cmd = args[0];

        args = args.splice(1);
        switch(cmd) {
            case 'raid':
                //345258048270630922 group finder
                //396153950757322762
                //472435867248754704 dev
                //417030346585014273 warriors

                if (channelID == '472435867248754704' || channelID == '345258048270630922' || channelID == '396153950757322764') {
                    //Search for user already registered for raid, or append roster
                    for (var x = 0; x < 24; x++) {
                        //If user is already in list, change role and stop looking
                        if (userID == roster.id[x]) {
                            roster.role[x] = args[0];
                            roster.added[x] = false;
                            roster.var1[x] = (args[1] == undefined) ? " " : args[1];
                            roster.var2[x] = (args[2] == undefined) ? " " : args[2];
                            roster.var3[x] = (args[3] == undefined) ? " " : args[3];
                            roster.var4[x] = (args[4] == undefined) ? " " : args[4];
                            roster.var5[x] = (args[5] == undefined) ? " " : args[5];
                            roster.var6[x] = (args[6] == undefined) ? " " : args[6];
                            roster.var7[x] = (args[7] == undefined) ? " " : args[7];
                            roster.var8[x] = (args[8] == undefined) ? " " : args[8];
                            found = true;
                            break;
                        }
                    }
                    //If user was not found, insert user and role into first empty slot
                    for (var x = 0; x < 24; x++) {
                        if ( roster.id[x] == null && found == false) {
                            roster.id[x] = userID;
                            roster.added[x] = false;
                            roster.role[x] = args[0];
                            roster.var1[x] = (args[1] == undefined) ? " " : args[1];
                            roster.var2[x] = (args[2] == undefined) ? " " : args[2];
                            roster.var3[x] = (args[3] == undefined) ? " " : args[3];
                            roster.var4[x] = (args[4] == undefined) ? " " : args[4];
                            roster.var5[x] = (args[5] == undefined) ? " " : args[5];
                            roster.var6[x] = (args[6] == undefined) ? " " : args[6];
                            roster.var7[x] = (args[7] == undefined) ? " " : args[7];
                            roster.var8[x] = (args[8] == undefined) ? " " : args[8];
                            break;
                        }
                    }
                    found = false;
                    //Init parsed list
                    parsed.damage = "\n**DAMAGE**";
                    //parsed.heals = "\n**HEALS**";
                    parsed.support = "\n**SUPPORT**";
                    parsed.pending = "\n**PENDING**";
                    //Sort roster into category lists
                    for ( var x = 0; x < 24; x++ ) {
                        if (roster.id[x] == null) {
                            continue;
                        }
                        if ( roster.role[x] == "damage") {
                            if (roster.added[x]) {
                                parsed.damage += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            } else {

                                parsed.damage += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            }

                        } else if (roster.role[x] == "heals" ) {

                            if (roster.added[x]) {
                                parsed.support += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            } else {

                                parsed.support += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            }

                        } else if (roster.role[x] == "support") {
                            if (roster.added[x]) {
                                parsed.support += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            } else {

                                parsed.support += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            }

                        } else {
                            if (roster.added[x]) {
                                parsed.pending += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            } else {

                                parsed.pending += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            }

                        }
                    }

                    //Count participants
                    for (var x = 0; x < 24; x++) {
                        if (roster.id[x] != null) {
                            count++;
                        }

                    }
                    bot.sendMessage({
                        to: channelID,
                        message: '<@'+userID+'> '+join.joining[joiningIdx]+'\n\n'+parsed.damage+'\n'+parsed.support+'\n'+parsed.pending+'\n*Attending:*  '+count
                    });


                    if (joiningIdx == 9) {
                        joiningIdx = 0;
                    } else {
                        joiningIdx++;
                    }
                    count = 0;
                } else {

                    bot.sendMessage({
                        to: channelID,
                        message: '<@'+userID+'> RaidBot is not responding on this channel right now.' });

                }

                break;
            case 'raidhelp':
                bot.sendMessage({
                    to: channelID,
                    message: ' ```css\nOC Raid Organizer expects the following format:\n\n!raid [damage,support,pending] followed by up to 8 words to describe your character (Magplar, DK, etc).\n\nThe ROLE field [damage, support, or pending] is the field used for sorting, excluding or misusing this field will result in being placed in the Pending list.\n\n**Command List:**\n!raid - Edits the existing roster\n!raidhelp - Displays this help message\n!raidshow - Displays the current roster\n!raidclear - Resets the roster (permissions to be added)\n!raiddrop - Removes you from the current roster\n\nExample: "!raid damage Magplar" will enter your user name in the DAMAGE role as "Magplar"\n``` '})

                break;
            case 'raidclear':
                if (channelID == '472435867248754704' || channelID == '345258048270630922' || channelID == '396153950757322764') {
                    //Initialize roster
                    for ( var x = 0; x < 24; x++) {
                        roster.id[x] = null;
                        roster.added[x] = false;
                        roster.var1[x] = null;
                        roster.var2[x] = null;
                        roster.var3[x] = null;
                        roster.var4[x] = null;
                        roster.var5[x] = null;
                        roster.var6[x] = null;
                        roster.var7[x] = null;
                        roster.var8[x] = null;
                        roster.role[x] = null;
                    }
                    //Init parsed list
                    parsed.damage = "\n**DAMAGE**";
                    //parsed.heals = "\n**HEALS**";
                    parsed.support = "\n**SUPPORT**";
                    parsed.pending = "\n**PENDING**";

                    bot.sendMessage({
                        to: channelID,
                        message: 'OC Raid Organizer has cleared the roster.'})
                    break;
                } else {

                    bot.sendMessage({
                        to: channelID,
                        message: '<@'+userID+'> RaidBot is not responding on this channel right now.' });

                }
            case 'raidshow':

                //Init parsed list
                parsed.damage = "\n**DAMAGE**";
                //parsed.heals = "\n**HEALS**";
                parsed.support = "\n**SUPPORT**";
                parsed.pending = "\n**PENDING**";
                //Sort roster into category lists
                for ( var x = 0; x < 24; x++ ) {
                    if (roster.id[x] == null) {
                        continue;
                    }
                    if ( roster.role[x] == "damage") {
                        if (roster.added[x]) {
                            parsed.damage += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                        } else {

                            parsed.damage += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                        }

                    } else if (roster.role[x] == "heals" ) {

                        if (roster.added[x]) {
                            parsed.support += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                        } else {

                            parsed.support += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                        }

                    } else if (roster.role[x] == "support") {
                        if (roster.added[x]) {
                            parsed.support += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                        } else {

                            parsed.support += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                        }

                    } else {
                        if (roster.added[x]) {
                            parsed.pending += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                        } else {

                            parsed.pending += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                        }

                    }
                }

                //Count participants
                for (var x = 0; x < 24; x++) {
                    if (roster.id[x] != null) {
                        count++;
                    }

                }

                bot.sendMessage({
                    to: channelID,
                    message: 'Displaying Raid List\n\n'+parsed.damage+'\n'+parsed.support+'\n'+parsed.pending+'\n*Attending:*  '+ count

                });

                count = 0;
                break;
            case 'raiddrop':
                if (channelID == '472435867248754704' || channelID == '345258048270630922' || channelID == '396153950757322764') {
                    for (var x = 0; x < 24; x++) {
                        //If user is  in list, clear slot and stop looking
                        if (userID == roster.id[x]) {
                            roster.id[x] = null;
                            roster.added[x] = false;
                            roster.role[x] = null;
                            roster.var1[x] = null;
                            roster.var2[x] = null;
                            roster.var3[x] = null;
                            roster.var4[x] = null;
                            roster.var5[x] = null;
                            roster.var6[x] = null;
                            roster.var7[x] = null;
                            roster.var8[x] = null;
                            found = true;
                            break;
                        }
                    }
                    if (found) {

                        //Init parsed list
                        parsed.damage = "\n**DAMAGE**";
                        //parsed.heals = "\n**HEALS**";
                        parsed.support = "\n**SUPPORT**";
                        parsed.pending = "\n**PENDING**";
                        //Sort roster into category lists
                        for ( var x = 0; x < 24; x++ ) {
                            if (roster.id[x] == null) {
                                continue;
                            }
                            if ( roster.role[x] == "damage") {
                                if (roster.added[x]) {
                                    parsed.damage += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                                } else {

                                    parsed.damage += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                                }

                            } else if (roster.role[x] == "heals" ) {

                                if (roster.added[x]) {
                                    parsed.support += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                                } else {

                                    parsed.support += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                                }

                            } else if (roster.role[x] == "support") {
                                if (roster.added[x]) {
                                    parsed.support += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                                } else {

                                    parsed.support += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                                }

                            } else {
                                if (roster.added[x]) {
                                    parsed.pending += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                                } else {

                                    parsed.pending += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                                }

                            }
                        }

                        //Count participants
                        for (var x = 0; x < 24; x++) {
                            if (roster.id[x] != null) {
                                count++;
                            }

                        }
                        bot.sendMessage({
                            to: channelID,
                            message: '<@'+userID+'> '+leave.leaving[leavingIdx]+'\n\n'+parsed.damage+'\n'+parsed.support+'\n'+parsed.pending+'\n*Attending:*  '+count
                        })
                        if (leavingIdx == 9) {
                            leavingIdx = 0;
                        } else {
                            leavingIdx++;
                        }
                        count = 0;
                        found = false;
                    } else {
                        bot.sendMessage({
                            to: channelID,
                            message: '<@'+userID+'>, you were not found on the roster.\n'
                        })

                    }
                } else {

                    bot.sendMessage({
                        to: channelID,
                        message: '<@'+userID+'> RaidBot is not responding on this channel right now.' });
                }

                break;
            //Add another member
            case 'raidadd':
                if (channelID == '472435867248754704' || channelID == '345258048270630922' || channelID == '396153950757322764') {
                    //Search for user already registered for raid, or append roster
                    for (var x = 0; x < 24; x++) {
                        //If user is already in list, change role and stop looking
                        if (args[1] == roster.id[x]) {
                            roster.role[x] = args[0];
                            roster.added[x] = true;
                            roster.var1[x] = (args[2] == undefined) ? " " : args[2];
                            roster.var2[x] = (args[3] == undefined) ? " " : args[3];
                            roster.var3[x] = (args[4] == undefined) ? " " : args[4];
                            roster.var4[x] = (args[5] == undefined) ? " " : args[5];
                            roster.var5[x] = (args[6] == undefined) ? " " : args[6];
                            roster.var6[x] = (args[7] == undefined) ? " " : args[7];
                            roster.var7[x] = (args[8] == undefined) ? " " : args[8];
                            roster.var8[x] = (args[9] == undefined) ? " " : " ";
                            found = true;
                            break;
                        }
                    }
                    //If user was not found, insert user and role into first empty slot
                    for (var x = 0; x < 24; x++) {
                        if ( roster.id[x] == null && found == false) {
                            roster.id[x] = args[1];
                            roster.added[x] = true;
                            roster.role[x] = args[0];
                            roster.var1[x] = (args[2] == undefined) ? " " : args[2];
                            roster.var2[x] = (args[3] == undefined) ? " " : args[3];
                            roster.var3[x] = (args[4] == undefined) ? " " : args[4];
                            roster.var4[x] = (args[5] == undefined) ? " " : args[5];
                            roster.var5[x] = (args[6] == undefined) ? " " : args[6];
                            roster.var6[x] = (args[7] == undefined) ? " " : args[7];
                            roster.var7[x] = (args[8] == undefined) ? " " : args[8];
                            roster.var8[x] = (args[9] == undefined) ? " " : " ";
                            break;
                        }
                    }
                    found = false;
                    //Init parsed list
                    parsed.damage = "\n**DAMAGE**";
                   //parsed.heals = "\n**HEALS**";
                    parsed.support = "\n**SUPPORT**";
                    parsed.pending = "\n**PENDING**";
                    //Sort roster into category lists
                    for ( var x = 0; x < 24; x++ ) {
                        if (roster.id[x] == null) {
                            continue;
                        }
                        if ( roster.role[x] == "damage") {
                            if (roster.added[x]) {
                                parsed.damage += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            } else {

                                parsed.damage += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            }

                        } else if (roster.role[x] == "heals" ) {

                            if (roster.added[x]) {
                                parsed.support += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            } else {

                                parsed.support += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            }

                        } else if (roster.role[x] == "support") {
                            if (roster.added[x]) {
                                parsed.support += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            } else {

                                parsed.support += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            }

                        } else {
                            if (roster.added[x]) {
                                parsed.pending += "\n"+roster.id[x]+" "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            } else {

                                parsed.pending += "\n<@"+roster.id[x]+"> "+roster.var1[x]+" "+roster.var2[x]+" "+roster.var3[x]+" "+roster.var4[x]+" "+roster.var5[x]+" "+roster.var6[x]+" "+roster.var7[x]+" "+roster.var8[x]+" \n";
                            }

                        }
                    }

                    //Count participants
                    for (var x = 0; x < 24; x++) {
                        if (roster.id[x] != null) {
                            count++;
                        }

                    }
                    bot.sendMessage({
                        to: channelID,
                        message: ''+args[1]+' '+join.joining[joiningIdx]+'\n\n'+parsed.damage+'\n'+parsed.support+'\n'+parsed.pending+'\n*Attending:*  '+count

                    });
                    if (joiningIdx == 9) {
                        joiningIdx = 0;
                    } else {
                        joiningIdx++;
                    }
                    count = 0;
                } else {

                    bot.sendMessage({
                        to: channelID,
                        message: '<@'+userID+'> RaidBot is not responding on this channel right now.' });
                }
                break;
            default:
                //ignore
                //channel user has sent a bad !raid command. Remind them of the correct syntax
                //bot.sendMessage({
                //    to: channelID,
                //    message: '!raidhelp' });

        }
    }
});